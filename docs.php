<?php exit;

$config->ext_linker->base_url = 'http://site.loc/';  // 

// Параметры для перелинковки
$config->ext_linker->relink_links_in_block    = 3;   // Всего ссылок в блоке
$config->ext_linker->relink_links_repeat      = 10;  // Кол-во повторений ссылок (всего)
$config->ext_linker->relink_main_links_repeat = 11;  // Кол-во повторений главных ссылок

// Параметры для контекстных ссылок
$config->ext_linker->context_exclude = array();      // ?Страницы где не проставлять ссылки
$config->ext_linker->context_targets = array();      // Список целей. Страницы где будут проставляться ссылки
$config->ext_linker->context_anchors_repeat = 1;     // Кол-во повторяющихся анкоров на странице
$config->ext_linker->context_links_repeat   = 99;    // Кол-во повторяющихся ссылок на странице
$config->ext_linker->context_links_per_page = 99;    // А в овраге речка а на речке мост на мосту овечка у овечки хвост
