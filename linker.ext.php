<?php



class EXT_Linker
{
	//--------------------------------------------------------------------------
	
	public $base_url = '';
	
	public $enable_relink  = TRUE;
	public $enable_context = TRUE;
	
	public $relink_links_in_block    = 3;
	public $relink_links_repeat      = 10;
	public $relink_main_links_repeat = 11;
//	public $exclude_pages = array(
//		'',
//		'users/login',
//		'faq/ask',
//		'illness',
//		'symptoms',
//		'inspect',
//		'drugs',
//	);
	public $context_exclude        = array();      // ?Страницы где не проставлять ссылки
	public $context_targets	       = array();      // Список целей. Страницы где будут проставляться ссылки
	public $context_anchors_repeat = 1;     // Кол-во повторяющихся анкоров на странице
	public $context_links_repeat   = 99;    // Кол-во повторяющихся ссылок на странице
	public $context_links_per_page = 99;    // А в овраге речка а на речке мост на мосту овечка у овечки хвост
	
	// Параметры "простановки ссылок"
	public $context_rows_per_process = 30;					// Кол-во записей обрабатываемы при одном запросе
	public $context_links_per_cycle  = 50;					// Кол-во ссылок обрабатываемы при одном цикле
	public $context_link_extra       = ' class="ext_context"';
	public $context_stop_tags        = array(
		array('<a ', '</a>'),
		array('<h' , '</h'),
		array('<' , '>'),
	);

	//--------------------------------------------------------------------------
	
	function __construct()
	{
		sys::set_base_objects(&$this);
		
		sys::set_config_items(&$this, 'ext_linker');

		if (CRON_MODE)
		{
			$this->context_process();
		}
	}
	
	//--------------------------------------------------------------------------
	
	public function render_block($block_id)
	{
		$uri_hash = md5($this->uri->uri_string);
		
		$block = $this->db->where('b.uri_hash=? AND b.block_id=?', $uri_hash, $block_id)->get('relink_blocks b')->row();
		
		if ($block)
		{
			$body = $block->body;
			$group = $this->db->where('id=?', $block->group_id)->limit(1)->get('relink_groups')->row();
			
			if ( ! $block->url && $this->uri->uri_string)
			{
				$this->db->where('id=?', $block->id)->update('relink_blocks', array('url'=>$this->uri->uri_string));
			}
		}
		// GENERATE BLOCK
		else
		{
			$this->db->order_by('RAND()');
			$group = $this->db->where('status=1 AND block_id=?', $block_id)->limit(1)->get('relink_groups')->row();
			
			if ( ! $group) return '';
			
			$main_link = $this->db->where('group_id=? AND main=1', $group->id)->order_by('insert_links')->limit($this->relink_links_in_block)->get('relink_links')->row();
			
			$count = $this->relink_links_in_block - ($main_link ? 1 : 0);
			$links = $main_link ? array($main_link) : array();
			$urls  = array();
			
			while ($count--)
			{
				$this->db->where('group_id=? AND main!=1 AND insert_links<?', $group->id, $this->relink_links_repeat)->order_by('insert_links')->limit(1);
				foreach ($urls as $url) $this->db->where('link!=?', $url);
				$row = $this->db->get('relink_links')->row();
				
				if ( ! $row) break;
				
				$links[] = $row;
				$urls[]  = $row->link;
			}
			
			if ( ! $links || (count($links)==1 && $links[0]->insert_links >= $this->relink_main_links_repeat))
			{
				$this->db->where('id=?', $group->id)->update('relink_groups', array('status'=>2));
				return '';
			}
			
			$body = '';
			$ids = array();
			foreach ($links as $row)
			{
				$ids[] = $row->id;
				$body .= "<li><a href=\"{$row->link}\">{$row->title}</a></li>";
			}
			
			$body = '<ul>' . $body . '</ul>';
			
			$this->db->where_in('id', $ids)->set('insert_links = insert_links+1')->update('relink_links');
			
			$this->db->insert('relink_blocks', array(
				'uri_hash' => $uri_hash,
				'block_id' => $block_id,
				'body'     => $body,
				'group_id' => $group->id,
				'url'      => $this->uri->uri_string
			));
		}
		
		return (object)array('title'=>&$group->title, 'body'=>&$body);
	}
	
	//--------------------------------------------------------------------------
	
	function reset_group($id)
	{
		$this->db->where('group_id=?', $id)->update('relink_links', array('insert_links'=>0));
		$this->db->where('id=?', $id)->update('relink_groups', array('status'=>1));
		$this->db->where('group_id=?', $id)->delete('relink_blocks');
	}
	
	//--------------------------------------------------------------------------
	
	function context_task_new()
	{
		$task_in_process = $this->db->where('status=0')->count_all('context_tasks');

		if ($task_in_process) return;

		$this->db->insert('context_tasks', array(
			'status'    => 0,
			'cur_model' => current(array_keys($this->context_targets)),
			'postdate'  => time()
		));

		$this->db->where('1=1')->set('insert_links=0, found_links=0')->update('context_links');
		$this->db->query('TRUNCATE TABLE context_pages');
	}

	//--------------------------------------------------------------------------

	function context_task_stop($id)
	{
		$this->db->where('id=?', $id)->set('status=1')->update('context_tasks');
	}

	//--------------------------------------------------------------------------

	function context_task_update($task)
	{
		$task->last_run = time();
		$this->db->where('id=?', $task->id)->update('context_tasks', (array)$task);
	}

	//--------------------------------------------------------------------------

	function context_task_next(&$task)
	{
		$old_model = '';

		foreach ($this->context_targets as $model_name => $opt)
		{
			if ($task->cur_model == $old_model)
			{
				$task->cur_model = $model_name;
				$task->cur_count = 0;
				$this->context_task_update($task);
				return TRUE;
			}

			$old_model = $model_name;
		}

		$task->status = 1;
		$this->context_task_stop($task->id);
		return FALSE;
	}

	//--------------------------------------------------------------------------

	// Возвращает ключ версии для истории (ext: versioning) изменений
	function context_task_version($id)
	{
		return 'ext_context_' . $id;
	}

	//--------------------------------------------------------------------------

	function context_store_link($page_url, $link_id, $group_id, $anchor = '', $href = '')
	{
		$this->db->insert('context_pages', array(
			'page_url' => $page_url,
			'link_id'  => $link_id,
			'group_id'  => $group_id,
			'anchor'   => $anchor,
			'href'     => $href
		));
	}

	//--------------------------------------------------------------------------

	function context_process()
	{
		if (FALSE)
		{
			$anchor = "анкор";
			$href   = "/href/";
			$tests  = array(
				"Анкор в теге (между<>)"   => "Проверка на <img src='' alt='анкор'> в теге. Второй анкор",
				"Анкор в запрещенном теге" => "<a href='#'>Проверка на анкор в запрещенном</a> теге. Второй анкор",
			);

			echo "DEBUG: <pre>";
			foreach ($tests as $name => $body)
			{
				echo "<hr><b>{$name}</b><br>";
				echo htmlspecialchars($body) . '<br>' . htmlspecialchars($this->context_put_links($body, $anchor, $href));
			}
			exit;
		}


		// Поиск заданий для обработки
		$task = $this->db->where('status=0 AND cur_model!=""')->order_by('id')->limit(1)->get('context_tasks')->row();

		if ( ! $task)
		{
			// Нет задач для выполнения
			return FALSE;
		}

		// Данные в которых нужно проставить КС
		do {
			$target_opt = $this->context_targets[$task->cur_model];

			$this->db->limit($task->cur_count, $this->context_rows_per_process);

			if (empty($target_opt['is_table']))
			{
				// Load model
				$model_name = $task->cur_model;
				if ( empty(sys::$model->$model_name)) sys::$lib->load->model($model_name);
				$model =& sys::$model->$model_name;

				$data = $model->get_result();
			}
			else
			{
				$data  = $this->db->get($task->cur_model)->result();
				$model = FALSE;
			}


			if ( ! $data)
			{
				if ( ! $this->context_task_next(&$task))
				{
					// Нет задач для выполнения
					return FALSE;
				}
			}
		} while ( ! $data);
		
		$data_key = empty($target_opt['data_key']) ? 'body' : $target_opt['data_key'];

		$index = 0;
		// Контекстные ссылки (КС) которые нужно проставить
		while (($result = $this->db->limit(($index++)*$this->context_links_per_cycle, $this->context_links_per_cycle)->order_by('group_id, strlen DESC')->get('context_links')->result()))
		{
			$context_links = array();
			$context_hrefs = array();
			foreach ($result as $i => $row)
			{
				$href = $this->clear_base_url($row->link);
				$context_links[$row->group_id][$row->id] =& $result[$i];
				$context_hrefs[$href][$row->id]          =& $result[$i];
			}

			// Данные компонента
			foreach ($data as $di => &$drow)
			{
				if ( ! isset($drow->is_modify))
				{
					if ( ! $model)
					{
						$full_link_tpl    = empty($target_opt['full_link']) ? array("/{$task->cur_model}/%d/", 'id') : $target_opt['full_link'];
						$drow->full_link  = $this->link_from_template($full_link_tpl, $drow);
					}
					else
					{
						$drow->full_link = strtolower(isset($drow->full_link) ? $drow->full_link : $drow->full_url);
					}

					// Ссылка на страницу с целевым текстом
					$drow->row_data   = $drow->$data_key;
					$drow->is_modify  = FALSE;
					$drow->data_links = array();
					
					// Находим имеющиеся ссылки
					$links_result = $this->context_find_links($drow->row_data);
					foreach ($links_result as $i => $link)
					{
						$drow->data_links[$link['href']] =& $links_result[$i];
					}
				}

				// КС которые проставляем. группировка по ссылке (href)
				foreach ($context_hrefs as $href => $group_links)
				{
					// Пропускаем если ссылки ведут на себя
					if ($drow->full_link == $href) continue;
					
					// анкоры групп
					foreach ($group_links as $link_id => $link)
					{
						// Ссылка и анкор уже есть в тексте. Пропускаем
						if (isset($drow->data_links[$href]) && $drow->data_links[$href]['anchor'] == $link->title)
						{
							$this->context_store_link($drow->full_link, $link->id, $link->group_id);

							// Инкремент кол-ва ссылок по данному анкору и ссылке
							$this->db->set($drow->data_links[$href]['context_link'] ? 'insert_links=insert_links+1' : 'found_links=found_links+1');
							$this->db->where('id=?', $link_id)->update('context_links');
							
							$task->found_links++;
						}
						// Вставляем ссылку если есть куда
						else
						{
							$new_data = $this->context_put_links($drow->row_data, $link->title, $href);

							// Ссылка была установлена
							if ($new_data)
							{
								$this->context_store_link($drow->full_link, $link_id, $link->group_id);

								// Инкримент значения insert_links (вставлено ссылок)
								$this->db->where('id=?', $link_id)->set('insert_links=insert_links+1')->update('context_links');
								$task->insert_links++;
								$drow->row_data  = $new_data;
								$drow->is_modify = TRUE;
							}
						}
					}
				}
			}
		}
		
		// Если были добавлены ссылки
		foreach ($data as $row)
		{
			if ( ! empty($row->is_modify))
			{
				$table = $model ? $model->table : $task->cur_model;
				// Сохраняем старую копию
				$this->load->extension('versioning')->add_version($table, $row->id, array($data_key=>$row->$data_key), $this->context_task_version($task->id));

				// Записываем данные
				$this->db->where('id=?', $row->id)->limit(1)->update($table, array($data_key=>$row->row_data));
			}
		}
		
		$task->read_rows += count($data);
		$task->cur_count += $this->context_rows_per_process;
		$this->context_task_update($task);

		return $task;
	}

	//--------------------------------------------------------------------------

	function link_from_template($opt, &$data)
	{
		foreach ($opt as $i=>$key) if ($i) $opt[$i] = $data->$key;
		return call_user_func_array('sprintf', $opt);
	}
	
	//--------------------------------------------------------------------------
	
	function context_put_links($data, $anchor, $href, $offset = 0)
	{
		$preg_anchor = preg_quote($anchor);

		$ldata = '';

		if ($offset)
		{
			$ldata = mb_substr($data, 0, $offset);
			$data  = mb_substr($data, $offset);
		}

		if (preg_match('/' . $preg_anchor . '/iu', $data, $matches))
		{

			$pos = mb_strpos($data, $matches[0]);

			// Наш анкор должен быть началом слова.
			// Т.е. Перед анкором не должно быть букв
			if ($pos)
			{
				if (preg_match("/[А-Яа-яA-Za-z0-9]/iu", mb_substr($data, $pos-1, 1)))
				{
					return FALSE;
				}
			}

			$r_data = mb_substr($data, $pos);

			// Находится ли анкор в запрещенном тэге
			foreach ($this->context_stop_tags as $i => $stop_tag)
			{
				$close_pos = mb_strpos($r_data, $stop_tag[1]);
				
				if ($close_pos !== FALSE)
				{
					$open_pos = mb_strpos($r_data, $stop_tag[0]);
					if ( ! $open_pos || $open_pos > $close_pos)
					{
						// Ищем другое вхождение ссылки
						return $this->context_put_links($data, $anchor, $href, $pos + mb_strlen($matches[0]));
					}
				}
			}
			$preg_replace = '<a href="' . $href . '"'.$this->context_link_extra.'>$1</a>';
			return $ldata . preg_replace('/(' . $preg_anchor . '[А-Яа-яA-Za-z0-9]*)/iu', $preg_replace, $data, 1);
		}

		return FALSE;
	}

	//--------------------------------------------------------------------------

	function context_find_links($body)
	{
		static $preg_link_extra;

		if ($preg_link_extra === NULL)
		{
			$preg_link_extra = '/' . preg_quote($this->context_link_extra) . '/iu';
		}

		$result = array();
		preg_match_all('/<a [^>]*href=["\']([^>]+?)["\'][^>]*>(.*?)<\/a>/i', $body, $matches);
		
		foreach ($matches[0] as $i => $links)
		{
			$result[] = array(
				'src'      => $matches[0][$i],
				'href'     => $matches[1][$i],
				'anchor'   => trim(mb_strtolower($matches[2][$i])),
				'ghref'    => $this->clear_base_url($matches[1][$i]),
				'context_link' => preg_match($preg_link_extra, $matches[0][$i])
			);
		}
		
		return $result;
	}

	//--------------------------------------------------------------------------

	function clear_base_url($url)
	{
		static $pregstr;

		if ($pregstr === NULL)
		{
			$pregstr = '/' . preg_quote($this->base_url, '/') . '/i';
		}

		return preg_replace($pregstr, '/', $url);
	}

	//--------------------------------------------------------------------------

	function relink_update_from_csv($filename, $block_id = 1)
	{
		$result = $this->_update_from_csv('relink', $filename, $block_id);
		
		// Проставляем ссылки MAIN
		$main_links = $this->db->where('main=1')->get('relink_links')->result();
		foreach ($main_links as $row)
		{
			$result['update_links'] += $this->db->where('link=?', $row->link)->update('relink_links', array('main'=>1));
		}
		
		return $result;
	}
	
	//--------------------------------------------------------------------------
	
	function context_update_from_csv($filename)
	{
		$result = $this->_update_from_csv('context', $filename);
		
		// Проставляем ссылки MAIN
		// $main_links = $this->db->where('main=1')->get('relink_links')->result();
		// foreach ($main_links as $row)
		// {
		// 	$result['update_links'] += $this->db->where('link=?', $row->link)->update('relink_links', array('main'=>1));
		// }
		
		return $result;
	}

	//--------------------------------------------------------------------------

	private function _update_from_csv($table_prefix, $filename, $block_id = 1)
	{
		$result = array(
			'update_groups' => 0,
			'insert_groups' => 0,
			'update_links'  => 0,
			'insert_links'  => 0,
			'error_links'   => 0,
			'reset_groups'  => 0,
		);
		
		
		$reset_groups = array();
		$group_key    = '';
		$group        = new StdClass;
		
		
		$fh = fopen($filename, 'r');
		while (($row = fgetcsv($fh)) !== FALSE)
		{
			$title = trim(mb_strtolower($row[0]));

			if ( ! $title) continue;
			
			$link = trim($row[1]);
			
			if ( ! $link)
			{
				//echo '<hr>' . $title . '<br>';
				$group_key = md5($title . $block_id);
				$group = $this->db->where('id_key=?', $group_key)->get("{$table_prefix}_groups")->row();
				
				$data = array(
					'title'    => $title,
					'block_id' => $block_id,
					'status'   => 1
				);

				if ($group)
				{
					$result['update_groups'] += $this->db->where('id_key=?', $group_key)->update("{$table_prefix}_groups", $data);
				}
				else
				{
					$data['id_key'] = $group_key;
					$insert_id = $this->db->insert("{$table_prefix}_groups", $data);
					if ($insert_id) $result['insert_groups']++;
					
					$data['id'] = $insert_id;
					
					$group = (object)$data;
				}
			}
			else
			{
				if ($group)
				{
					$key_id = md5($link . $title);
					
					$data = array(
						'key_id'   => $key_id,
						'group_id' => $group->id,
						'title'    => $title,
						'link'     => $link,
						'main'     => ($title == $group->title)+0,
						'strlen'   => mb_strlen($title)
					);
					
					$exists_link = $this->db->where('group_id=? AND key_id=?', $group->id, $key_id)->get("{$table_prefix}_links")->row();
					
					if ($exists_link && $exists_link->main) $data['main'] = 1;
					
					if ( ! $exists_link)
					{
						$insert_id = $this->db->insert("{$table_prefix}_links", $data);
						
						if ($insert_id) $result['insert_links']++;
						
						// ЕСЛИ НОВАЯ ССЫЛКА - РЕГЕНЕРИРУЕМ БЛОКИ
						$reset_groups[$group->id] = $group->id;
					}
					else
					{
						$update = FALSE;
						foreach ($data as $key=>$val)
						{
							if ($exists_link->$key != $val)
							{
								$update = TRUE;
								//echo $exists_link->id . ':' . $key . ' ' . $exists_link->$key . ' ' . $val . '<br>';
							}
						}
						
						if ($update)
						{
							$result['update_links'] += $this->db->where('id=?', $exists_link->id)->update("{$table_prefix}_links", $data);
						}
					}
					
				}
				else
				{
					$result['error_links']++;
				}
			}
		}
		fclose($fh);

		foreach ($reset_groups as $id)
		{
			$this->reset_group($id);
			$result['reset_groups']++;
		}

		return $result;
	}
	
	//--------------------------------------------------------------------------
	
}