<?php



class COM_Linker extends SYS_Component
{
	//--------------------------------------------------------------------------
	
	public $section_link = '/admin/linker/';
	
	//--------------------------------------------------------------------------
	
	function init()
	{
		$this->load->extension('Linker');
//		$this->admin->backend_sub_menu = array();
		if ($this->linker->enable_relink)  $this->admin->backend_sub_menu[$this->section_link . 'relink/']  = 'Перелинковка';
		if ($this->linker->enable_context) $this->admin->backend_sub_menu[$this->section_link . 'context/'] = 'Контекстные ссылки';
	}
	
	//--------------------------------------------------------------------------
	
	function index()
	{
		if ( ! $this->admin->backend_sub_menu) sys::error_404();
		hlp::redirect(current(array_keys($this->admin->backend_sub_menu)));
	}
	
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	
	function act_relink()
	{
		$args = func_get_args();
		return $this->_sub('relink', $args);
	}
	
	//--------------------------------------------------------------------------
	
	function relink_index()
	{
		$data = $this->db
			->select('g.*, COUNT(l.id) AS links, SUM(l.insert_links) AS insert_links')
			->join('relink_links l', 'l.group_id = g.id')
			->group_by('g.id')
			->order_by('g.title')
			->get('relink_groups g')->result();
		
		$stat = $this->db->select('COUNT(*) AS links, SUM(insert_links) AS insert_links')->get('relink_links')->row();
		
		$stat->blocks = $this->db->count_all('relink_blocks');
		$stat->pages  = $this->db->query('SELECT COUNT(DISTINCT uri_hash) AS c FROM `relink_blocks`;')->row()->c;

		$this->data['data'] =& $data;
		$this->data['stat'] =& $stat;
	}
	
	//--------------------------------------------------------------------------
	
	function relink_upload()
	{
		$this->load->library('upload');
		
		$this->upload->set_field('file');
		$this->upload->set_allowed_types('csv');
		$this->upload->set_upload_path('temp');
		$this->upload->set_file_name('relink');

		$csv_file = $_SERVER['DOCUMENT_ROOT'].'/temp/relink.csv';
		
		if (file_exists($csv_file)) unlink($csv_file);
		
		if ($this->upload->run())
		{
			$result = $this->linker->relink_update_from_csv($csv_file, $_POST['block_id']);
		}
		
		$this->data['result'] = isset($result) ? $result : array();
	}
	
	//--------------------------------------------------------------------------
	
	function relink_group($id)
	{
		$group = $this->db->where('id=?', $id)->get('relink_groups')->row();
		
		if ( ! $group)
		{
			return sys::error_404();
		}
		$blocks = $this->db->where('group_id=?', $id)->get('relink_blocks')->result();
		$data   = $this->db->where('group_id=?', $id)->get('relink_links')->result();
		
		$this->data['data']   =& $data;
		$this->data['blocks'] =& $blocks;
		$this->data['group']  =& $group;
	}
	
	//--------------------------------------------------------------------------
	
	function relink_reset_group($id)
	{
		$this->view = FALSE;
		$this->linker->reset_group($id);
		hlp::redirect_back();
	}
	
	//--------------------------------------------------------------------------
	
	function relink_delete_all()
	{
		$this->view = FALSE;
		
		$this->db->where('id>0')->delete('relink_links');
		$this->db->where('id>0')->delete('relink_groups');
		$this->db->where('id>0')->delete('relink_blocks');
		
		hlp::redirect_back();
	}
	
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	
	function act_context()
	{
		$args = func_get_args();
		return $this->_sub('context', $args);
	}

	//--------------------------------------------------------------------------

	function context_index()
	{
		$data = $this->db
			->select('g.*, l.link, COUNT(l.id) AS links, SUM(l.insert_links) AS insert_links, SUM(l.found_links) AS found_links')
			->join('context_links l', 'l.group_id = g.id')
			->group_by('g.id')
			->order_by('insert_links DESC')
			->get('context_groups g')->result();
		
		$stat = $this->db->select('COUNT(*) AS links, SUM(insert_links) AS insert_links, SUM(found_links) AS found_links')->get('context_links')->row();
		
		// Кол-во записей истории
		$result = $this->db->select('version, COUNT(id) AS counts')->group_by('version')->get('versioning')->result();
		foreach ($result as $row)
		{
			$ver_counts[$row->version] = $row->counts;
		}

		$tasks = $this->db->order_by('id DESC')->get('context_tasks')->result();
		foreach ($tasks as $row)
		{
			$vkey = $this->linker->context_task_version($row->id);
			$row->ver_count = isset($ver_counts[$vkey]) ? $ver_counts[$vkey] : 0;
			$versions[$row->id] = '#' . $row->id . ') Записей: ' . $row->ver_count . ' | ' . hlp::date($row->postdate);
		}

		// View
		$this->data['data']     =& $data;
		$this->data['stat']     =& $stat;
		$this->data['tasks']    =& $tasks;
		$this->data['versions'] =& $versions;
		$this->data['ver_counts'] =& $ver_counts;
	}

	//--------------------------------------------------------------------------

	function context_group($id)
	{
		$group = $this->db->where('id=?', $id)->get('context_groups')->row();

		if ( ! $group) sys::error_404();

		$links = $this->db->where('group_id=?', $group->id)->order_by('insert_links DESC')->get('context_links')->result();
		$result = $this->db->where('group_id=?', $group->id)->get('context_pages')->result();
		foreach ($result as $row)
		{
			$pages[$row->link_id][] = $row;
		}

		$this->data['group'] =& $group;
		$this->data['links'] =& $links;
		$this->data['pages'] =& $pages;
	}

	//--------------------------------------------------------------------------

	function context_process($ajax = FALSE)
	{
		if ($ajax)
		{
			if ( ! FF_DEVMODE) exit();
			
			$this->template->enable = FALSE;
			$this->view = FALSE;

			$result = $this->linker->context_process();
			if ($result)
			{
				$result->postdate = hlp::date($result->postdate);
				$result->last_run = hlp::nicetime($result->last_run);
			}
			echo json_encode((array)$result);
		}
		else
		{
			$this->linker->context_task_new();
		}
	}
	
	//--------------------------------------------------------------------------

	function context_remove_task($id)
	{
		$this->db->where('id=?', $id)->delete('context_tasks');

		hlp::redirect_back();
	}

	//--------------------------------------------------------------------------

	function context_restore($version)
	{
		if ( ! empty($_POST['version']))
		{
			$version = isset($_POST['version']) ? $_POST['version'] : 0;
		}

		if ( ! $version) hlp::redirect_back();

		$version = $this->linker->context_task_version($version);

		$this->data['data'] = $this->load->extension('versioning')->restore_all($version);
	}

	//--------------------------------------------------------------------------

	function context_upload()
	{
		$this->load->library('upload');
		
		$this->upload->set_field('file');
		$this->upload->set_allowed_types('csv');
		$this->upload->set_upload_path('temp');
		$this->upload->set_file_name('linker');

		$csv_file = $_SERVER['DOCUMENT_ROOT'].'/temp/linker.csv';
		
		if (file_exists($csv_file)) unlink($csv_file);
		
		if ($this->upload->run())
		{
			$result = $this->linker->context_update_from_csv($csv_file);
		}

		// View
		$this->data['result'] =& $result;
	}
	
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	//--------------------------------------------------------------------------
	
	function _sub($section, $args)
	{
		$enable = 'enable_' . $section;
		if ( ! $this->linker->$enable) sys::error_404();
				
		$this->section_link .= $section . '/';
		
		$method    = empty($args[0]) ? 'index' : array_shift($args);
		$method_fn = $section . '_' . $method;
		
		$this->view = $section . '/' . $method;
		
		if (method_exists($this, $method_fn))
		{
			return call_user_func_array(array(&$this, $method_fn), $args);
		}
	}
	
	//--------------------------------------------------------------------------
	
}