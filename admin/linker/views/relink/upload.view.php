<? if (count($result)): ?>
	<div class="message">
		<b>ОБНОВЛЕНИЕ ЗАВЕРШЕНО</b><br />
		Новых групп: <?=$result['insert_groups'] ?><br />
		Обновлено групп: <?=$result['update_groups'] ?><br />
		Групп на регенирацию: <?=$result['reset_groups'] ?><br />
		Добавлено ссылок: <?=$result['insert_links'] ?><br />
		Обновлено ссылок: <?=$result['update_links'] ?>
	</div>
<? endif ?>

<?=h_form::open_multipart() ?>
	
	<?=h_form::file('file') ?>
	<?=h_form::select('block_id', array(1=>1,2=>2,3=>3)) ?>
	<?=h_form::submit('Upload') ?>
	
<?=h_form::close() ?>

<b>Формат файла CSV:</b><br />
<table class="data_table">
	<tr class="a"><td>Кодировка:</td><td>UTF-8</td><td></td></tr>
	<tr class="b"><td>Разделитель:</td><td>,</td><td><i>символ запятой</i></td></tr>
	<tr class="a"><td>Текстовый разделитель:</td><td>"</td><td><i>двойные кавычки</i></td></tr>
</table>
<br />

