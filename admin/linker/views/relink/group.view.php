<h1><?=$group->title ?> (Блок: <?=$group->block_id ?>)</h1>

<h2>Ссылки</h2>
<? if ($data): ?>
	<table class="data_table">
	<tr>
		<th>ID</th>
		<th>Заголовок</th>
		<th>Ссылка</th>
		<th>Вставлено</th>
		<th>Главная</th>
	</tr>
	<? foreach ($data as $i => $row): ?>
	<tr class="<?=$i%2==0?'a':'b' ?>">
		<td><?=$row->id ?></td>
		<td><?=$row->title ?></td>
		<td><?=$row->link ?></td>
		<td><?=$row->insert_links ?></td>
		<td><?=$row->main ? 'ДА' : '' ?></td>
	</tr>
	<? endforeach ?>
	</table>
<? else: ?>
	<em>НЕТ</em>
<? endif ?>


<h2>Блоки</h2>
<? if ($blocks): ?>
	<table class="data_table">
	<tr>
		<th>ID</th>
		<th>Ссылка</th>
	</tr>
	<? foreach ($blocks as $i => $row): ?>
	<tr class="<?=$i%2==0?'a':'b' ?>">
		<td><?=$row->id ?></td>
		<td>
		<? if ($row->url): ?>
			<a href="/<?=$row->url ?>/">/<?=$row->url ?>/</a>
		<? else: ?>
			?
		<? endif ?>
		</td>
	</tr>
	<? endforeach ?>
	</table>
<? else: ?>
	<em>НЕТ</em>
<? endif ?>