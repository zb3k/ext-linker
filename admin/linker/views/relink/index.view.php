<?php 
	$sstyle = array('color:#999','color:#900','background:#7D7');
	$snames  = array('Отключен', 'Обработка', 'Завершено');
?>

<a href="<?=$this->section_link ?>upload/" class="button">Импорт ссылок</a>
<a href="<?=$this->section_link ?>delete_all/" class="button" style="background:#F99; color:#700" onclick="return confirm('Уверены?!!!')">Удалить все ссылки</a>

<table class="data_table">
<tr class="a"><td>Сгенерировано блоков</td><td><?=$stat->blocks ?></td></tr>
<tr class="b"><td>Ссылок в БД</td><td><?=$stat->links ?></td></tr>
<tr class="a"><td>Расставлено ссылок</td><td><?=$stat->insert_links ?></td></tr>
<tr class="a"><td>Задействовано страниц</td><td><?=$stat->pages ?></td></tr>
</table>

<table class="data_table">
<tr>
	<th>ID</th>
	<th>Название</th>
	<th>Ссылок</th>
	<th>Блок</th>
	<th>Проставлено</th>
	<th>Статус</th>
	<th>Действия</th>
</tr>
<? foreach ($data as $i => $row): ?>
<tr class="<?=$i%2==0?'a':'b' ?>">
	<td><?=$row->id ?></td>
	<td><a href="<?=$this->section_link ?>group/<?=$row->id ?>/"><?=$row->title ?></a></td>
	<td><?=$row->links ?></td>
	<td><?=$row->block_id ?></td>
	<td><?=$row->insert_links ?></td>
	<td style="<?=$sstyle[$row->status] ?>"><?=$snames[$row->status] ?></td>
	<td><a href="<?=$this->section_link ?>reset_group/<?=$row->id ?>/">Сброс</a></td>
</tr>
<? endforeach ?>
</table>