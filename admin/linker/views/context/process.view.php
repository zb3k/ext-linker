<div id="time"></div>
<div id="iter"></div>
<div style="margin:10px 0 0 0; border:1px solid rgba(0,0,0,.2); width:30px; overflow:hidden; border-radius:10px; background:rgba(0,0,0,.1)">
	<div id="proc" style="background:#C00; height:5px; width:15px;"></div>
	</div>
<hr>
<div id="result">...</div>

<script type="text/javascript">
	var $result = $('#result');
	var $time   = $('#time');
	var $iter   = $('#iter');
	var $proc   = $('#proc');
	
	var time = {h:0,m:0,s:0};
	var iterations = 0;

	function render_time()
	{
		time.s++;
		if (time.s==60)
		{
			time.s = 0;
			time.m++;
			if (time.m==60)
			{
				time.m = 0;
				time.h++;
			}
		}

		$time.html("Время работы скрипта: ");
		for (var k in time) $time.append( (time[k]<10?'0':'') + time[k] + (k!='s'?':':'') );
	}

	var timeout;
	var interval = setInterval(render_time, 1000);

	function process()
	{
		iterations++;
		$iter.html("Итерации: " + iterations);

		$proc.animate({width: Math.abs(Math.sin(iterations)*35) });

		$.getJSON('/admin/linker/context/process/ajax/', 
		function(data){
			if (data && data.status && data.status == 0)
			{
				var html  = "<table class='data_table'>";
				var index = 0;
				for (var k in data)
				{
					html += "<tr class='"+(index%2==0?'a':'b')+"'><td>" + k + ": </td><td>" + data[k] + "</td></tr>";
					index++;
				}
				html += "</table>";
				$result.html(html);
				timeout = setTimeout(process, 200);
			}
			else
			{
				clearInterval(interval);
				$result.append("<br>ЗАВЕРШЕНО!");
			}
		});
	}

	process();
</script>