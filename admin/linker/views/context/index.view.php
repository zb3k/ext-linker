<?php 
	$sstyle = array('color:#C70','background:#7D7');
	$snames  = array('Обработка…', 'Завершено');
?>

<a href="<?=$this->section_link ?>upload/" class="button">Импорт ссылок</a>
<a onclick="return confirm('Вы уверены?')" href="<?=$this->section_link ?>process/" class="button">Проставить ссылки</a>

<br><br>
<h1>Задачи</h1>
<div>
	<? /*<?=h_form::open("{$this->section_link}restore/") ?>
	<b>Резервные копии</b>
	<?=h_form::select('version', $versions) ?>
	<button onclick="return confirm('Вы уверены?')" type="submit">Восстановить</button>
	<?=h_form::close() ?>
	*/ ?>
	<div style="width:99%; border:3px solid #CCC;">
	<table class="data_table" style="width:100%; margin:0">
		<tr>
			<th>ID</th>
			<th>Создан</th>
			<th>Последний запуск</th>
			<th>Обработано</th>
			<th>Установлено ссылок</th>
			<th>Найдено ссылок</th>
			<th>Статус</th>
			<th width=170>Восстановление данных</th>
			<th></th>
		</tr>
	<? foreach ($tasks as $i => $row): ?>
		<tr class="<?=$i%2==0?'a':'b' ?>">
			<td>#<?=$row->id ?></td>
			<td nowrap=nowrap><?=hlp::date($row->postdate) ?></td>
			<td nowrap=nowrap><?=hlp::nicetime($row->last_run) ?></td>
			<td><?=($row->read_rows) ?></td>
			<td><?=($row->insert_links) ?></td>
			<td><?=($row->found_links) ?></td>
			<td style="<?=$sstyle[$row->status] ?>"><?=$snames[$row->status] ?></td>
			<td nowrap=nowrap>
				Записей: <?=$row->ver_count ?>
				[<a onclick="return confirm('Вы уверены?')" href="<?=$this->section_link ?>restore/<?=$row->id ?>/">Восстановить</a>]
			</td>
			<td><a onclick="return confirm('Вы уверены?')" href="<?=$this->section_link ?>remove_task/<?=$row->id ?>/" style="color:#C00">удалить</a></td>
		</tr>
	<? endforeach ?>
	</table>
	</div>
</div>

<br>
<h1>Ссылки</h1>
<table class="data_table" style="margin:0; width:100%">
<tr>
	<th>ID</th>
	<th>Название</th>
	<th>Ссылка</th>
	<th>Анкоров</th>
	<th>Проставлено</th>
	<th>Найдено</th>
	<!-- <th>Действия</th> -->
</tr>
<tr style="font-size:15px">
	<th style="text-align:right; padding-right:10px" colspan=3>ВСЕГО:</th>
	<th style="color:#D70"><?=$stat->links ?></th>
	<th style="color:#D70"><?=$stat->insert_links ?></th>
	<th style="color:#D70"><?=$stat->found_links ?></th>
</tr>
<? foreach ($data as $i => $row): ?>
<tr class="<?=$i%2==0?'a':'b' ?>">
	<td><?=$row->id ?></td>
	<td><a href="<?=$this->section_link ?>group/<?=$row->id ?>/"><?=$row->title ?></a></td>
	<td><a href="<?=$row->link ?>" target="_blank"><?=$row->link ?></a></td>
	<td><?=$row->links ?></td>
	<td><?=$row->insert_links ?></td>
	<td><?=$row->found_links ?></td>
	<!-- <td><a href="<?=$this->section_link ?>reset_group/<?=$row->id ?>/">Сброс</a></td> -->
</tr>
<? endforeach ?>
</table>