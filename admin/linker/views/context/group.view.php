<h1>Группа: <?=$group->title ?></h1>

<table class="data_table">
<tr>
	<th>ID</th>
	<th></th>
	<th>Проставлено</th>
	<th>Найдено</th>
</tr>
<? foreach ($links as $row): ?>
<tr>
	<th><?=$row->id ?></th>
	<th><?=$row->title ?></th>
	<th><?=$row->insert_links ?></th>
	<th><?=$row->found_links ?></th>
</tr>
	<? if ( ! empty($pages[$row->id])): ?>
	<? foreach ($pages[$row->id] as $i=>$page): ?>
		<tr class="<?=$i%2==0?'a':'b'?>">
			<td></td>
			<td colspan="3">
				<a href="<?=$page->page_url ?>" target="_blank"><?=$page->page_url ?></a>
			</td>
		</tr>
	<? endforeach ?>
	<? endif ?>
<? endforeach ?>
</table>